from django.db import models


class Account(models.Model):
    """
    This model represents an Account. An Account is a group of users who share the same
    set of trees. Each Account has a name, creation date, and an active status.
    """
    name = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name
