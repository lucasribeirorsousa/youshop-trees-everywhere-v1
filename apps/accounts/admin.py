from django.contrib import admin
from apps.accounts.models import Account


class AccountAdmin(admin.ModelAdmin):
    actions = ['delete_selected','activate_accounts', 'deactivate_accounts']

    def delete_selected(modeladmin, request, queryset):
        for obj in queryset:
            # Adicione aqui a lógica personalizada de exclusão
            obj.delete()
    delete_selected.short_description = "Excluir contas selecionadas"

    def activate_accounts(self, request, queryset):
        queryset.update(is_active=True)
    activate_accounts.short_description = "Ativar contas selecionadas"

    def deactivate_accounts(self, request, queryset):
        queryset.update(is_active=False)
    deactivate_accounts.short_description = "Desativar contas selecionadas"


admin.site.register(Account, AccountAdmin)
