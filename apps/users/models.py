from config.wsgi import *
from django.db import models
from django.contrib.auth.models import User
from apps.accounts.models import Account
from apps.planted_trees.models import PlantedTree


class UserExtended(User):
    """
    This class extends Django's user class, providing additional methods for planting trees.
    """
    def plant_tree(self, tree, location):
        """
        This method adds a new tree planted by the user.
        """
        planted_tree = PlantedTree(user=self.userprofile, tree=tree, latitude=location[0], longitude=location[1])
        planted_tree.save()
    
    def plant_trees(self, planted_trees):
        """
        This method adds several new trees planted by the user.
        """
        for tree in planted_trees:
            tree, location = tree
            planted_tree = PlantedTree(user=self.userprofile, tree=tree, latitude=location[0], longitude=location[1])
            planted_tree.save()
    
    def __str__(self):
        return self.username


class UserProfile(models.Model):
    """
    The UserProfile model represents a profile for a user.
    
    Attributes:
        user (django.contrib.auth.models.User): The user this profile belongs to.
        about (models.TextField): A brief description of the user.
        joined (models.DateTimeField): The date and time this profile was created.
        accounts (models.ManyToManyField): The accounts this user belongs to.
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    about = models.TextField()
    joined = models.DateTimeField(auto_now_add=True)
    accounts = models.ManyToManyField(Account, related_name='users')

    def __str__(self):
        return self.user.username