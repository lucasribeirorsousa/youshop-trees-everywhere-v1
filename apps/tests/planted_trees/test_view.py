from config.wsgi import *
from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from apps.accounts.models import Account
from apps.trees.models import Tree
from apps.users.models import UserProfile
from apps.planted_trees.models import PlantedTree
from http import HTTPStatus


class TestTreePlanting(TestCase):
    def setUp(self):
        self.account1 = Account.objects.create(name="Conta 1")
        self.account2 = Account.objects.create(name="Conta 2")

        self.User = get_user_model()
        self.user1 = self.User.objects.create_user(username="usuario1", password="senha1")
        self.userprofile1 = UserProfile.objects.create(user=self.user1, about="Sobre o usuário 1")
        self.userprofile1.accounts.add(self.account1)
        self.user2 = self.User.objects.create_user(username="usuario2", password="senha2")
        self.userprofile2 = UserProfile.objects.create(user=self.user2, about="Sobre o usuário 2")
        self.userprofile2.accounts.add(self.account1, self.account2)
        self.user3 = self.User.objects.create_user(username="usuario3", password="senha3")
        self.userprofile3 = UserProfile.objects.create(user=self.user3, about="Sobre o usuário 3")
        self.userprofile3.accounts.add(self.account2)

        self.tree1 = Tree.objects.create(name="Árvore 1", scientific_name="Nome científico da Árvore 1")
        self.tree2 = Tree.objects.create(name="Árvore 2", scientific_name="Nome científico da Árvore 2")
        self.tree3 = Tree.objects.create(name="Árvore 3", scientific_name="Nome científico da Árvore 3")

        self.planted_tree1 = PlantedTree.objects.create(age=5, user=self.account1, tree=self.tree1, latitude=10.0, longitude=20.0, account=self.account1)
        self.planted_tree2 = PlantedTree.objects.create(age=3, user=self.account2, tree=self.tree1, latitude=15.0, longitude=25.0, account=self.account1)
        self.planted_tree3 = PlantedTree.objects.create(age=7, user=self.account2, tree=self.tree2, latitude=20.0, longitude=30.0, account=self.account2)
        self.planted_tree4 = PlantedTree.objects.create(age=2, user=self.account1, tree=self.tree3, latitude=25.0, longitude=35.0, account=self.account2)

        self.url = reverse('planted_trees:list', kwargs={'username': self.user_profile.username})

    def test_account_tree_list(self):
        # login
        self.client.login(username='user_test', password='testpassword')

        # add user accounts
        self.account1.users.add(self.user1.userprofile)
        self.account2.users.add(self.user2.userprofile)

        # get in list tree
        response = self.client.get(reverse('account_tree_list'))

        # verify status HTTP 200 (OK)
        self.assertEqual(response.status_code, 200)

        # verify planted trees by user accounts
        tree_list = response.context['tree_list']
        self.assertEqual(len(tree_list), 2)
        self.assertIn(self.planted_tree1, tree_list)
        self.assertIn(self.planted_tree2, tree_list)
        self.assertNotIn(self.planted_tree3, tree_list)
        self.assertNotIn(self.planted_tree4, tree_list)

    def test_planted_trees_list(self):
        # login user1
        self.client.login(username='user1', password='pass1')

        # access view
        response = self.client.get(reverse('planted_trees_list', args=[self.user1_profile.pk]))

        self.assertEqual(response.status_code, HTTPStatus.OK)

        self.assertContains(response, self.tree1.age)
        self.assertContains(response, self.tree2.age)

    def test_planted_trees_list_forbidden(self):
        # login user1
        self.client.login(username='user1', password='pass1')

        # access view
        response = self.client.get(reverse('planted_trees_list', args=[self.user2_profile.pk]))

        # verify erro 403 (Forbidden)
        self.assertEqual(response.status_code, HTTPStatus.FORBIDDEN)
    
    def test_planted_tree_list_template(self):
        response = self.client.get(self.url)
        self.assertTemplateUsed(response, 'planted_trees/planted_tree_list.html')
        self.assertContains(response, self.user_profile.username)
        self.assertContains(response, self.planted_tree.tree.name)
        self.assertContains(response, self.planted_tree.age)
        self.assertContains(response, self.planted_tree.location)
    