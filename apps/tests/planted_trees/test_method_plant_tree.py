from django.test import TestCase
from django.contrib.auth.models import User
from apps.planted_trees.models import PlantedTree
from apps.users.models import UserProfile
from apps.trees.models import Tree

class UserTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser',
            password='testpassword',
        )
        self.user_profile = UserProfile.objects.create(
            user=self.user,
            about='Test user profile',
        )

    def test_plant_tree(self):
        # Plant a new tree for the user
        tree = Tree.objects.create(name='Test Tree', description='Test tree description')
        location = (10.123456, 20.123456)
        self.user_profile.plant_tree(tree, location)

        # Check if the tree was planted for the user
        planted_tree = PlantedTree.objects.get(user=self.user_profile)
        self.assertEqual(planted_tree.tree, tree)
        self.assertEqual(planted_tree.latitude, location[0])
        self.assertEqual(planted_tree.longitude, location[1])
