from django.test import TestCase
from django.contrib.auth.models import User
from apps.planted_trees.models import PlantedTree
from apps.users.models import UserProfile
from apps.trees.models import Tree

class UserTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser',
            password='testpassword',
        )
        self.user_profile = UserProfile.objects.create(
            user=self.user,
            about='Test user profile',
        )

    def test_plant_trees(self):
        # Plant multiple trees for the user
        trees = [
            (Tree.objects.create(name='Test Tree 1', description='Test tree description 1'), (10.123456, 20.123456)),
            (Tree.objects.create(name='Test Tree 2', description='Test tree description 2'), (30.123456, 40.123456)),
        ]
        self.user_profile.plant_trees(trees)

        # Check if the trees were planted for the user
        planted_trees = PlantedTree.objects.filter(user=self.user_profile)
        self.assertEqual(planted_trees.count(), 2)
        self.assertEqual(planted_trees[0].tree, trees[0][0])
        self.assertEqual(planted_trees[0].latitude, trees[0][1][0])
        self.assertEqual(planted_trees[0].longitude, trees[0][1][1])
        self.assertEqual(planted_trees[1].tree, trees[1][0])
        self.assertEqual(planted_trees[1].latitude, trees[1][1][0])
        self.assertEqual(planted_trees[1].longitude, trees[1][1][1])
