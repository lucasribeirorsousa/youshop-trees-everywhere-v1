from django.contrib import admin
from apps.planted_trees.models import PlantedTree
from django.contrib.admin import SimpleListFilter
from apps.planted_trees.models import PlantedTree
from apps.trees.models import Tree

class TreeFilter(SimpleListFilter):
    title = 'Tree'
    parameter_name = 'tree'

    def lookups(self, request, model_admin):
        trees = Tree.objects.all()
        return [(tree.id, tree.name) for tree in trees]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(tree=self.value())
        return queryset


class PlantedTreeAdmin(admin.ModelAdmin):
    list_display = ('tree', 'user', 'planted_at')
    list_filter = [TreeFilter]

    def tree_name(self, obj):
        return obj.tree.name
    tree_name.short_description = 'Nome da Planta'

    def user_name(self, obj):
        return obj.user.username
    user_name.short_description = 'Nome do Usuário'

admin.site.register(PlantedTree, PlantedTreeAdmin)
