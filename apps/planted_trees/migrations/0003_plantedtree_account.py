# Generated by Django 4.1.6 on 2023-02-16 00:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
        ('planted_trees', '0002_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='plantedtree',
            name='account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='planted_trees_in', to='accounts.account'),
            preserve_default=False,
        ),
    ]
