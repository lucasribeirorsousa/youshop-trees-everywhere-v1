from django.db import models
from apps.users.models import UserProfile
from apps.trees.models import Tree
from apps.accounts.models import Account


class PlantedTree(models.Model):
    """
    Class PlantedTree represents a tree that has been planted by a user.

    Attributes:
    age (int): age of the tree in years.
    planted_at (DateTimeField): date and time when the tree was planted.
    user (ForeignKey): ForeignKey to the UserProfile that planted the tree.
    tree (ForeignKey): ForeignKey to the Tree model representing the species of the tree.
    location (DecimalField): geographical location of the planted tree.
    account (ForeignKey): ForeignKey to the Account model representing the account where the tree was planted.


    Methods:
    str: Returns the string representation of the UserProfile that planted the tree.
    """
    age = models.IntegerField()
    planted_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE, related_name='planted_trees_by')
    tree = models.ForeignKey(Tree, on_delete=models.CASCADE, related_name='planted_species')
    latitude = models.DecimalField(max_digits=10, decimal_places=7)
    longitude = models.DecimalField(max_digits=10, decimal_places=7)
    account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='planted_trees_in')


    @property
    def location(self):
        return (self.latitude, self.longitude)
        
    def __str__(self):
        return self.tree.name