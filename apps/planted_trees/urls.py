from django.urls import path
from apps.planted_trees.views import home_view, my_accounts, planted_tree_detail_view, PlantedTreeCreateView

app_name = 'planted_trees'

urlpatterns = [
    path('home/', home_view, name='home'),
    path('my_accounts/', my_accounts, name='my_accounts'),
    path('<int:id>/', planted_tree_detail_view, name='planted_tree_detail'),
    path('add/', PlantedTreeCreateView.as_view(), name='planted_tree_create'),
]