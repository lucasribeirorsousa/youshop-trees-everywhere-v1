from django import forms
from apps.trees.models import Tree
from apps.planted_trees.models import PlantedTree
from apps.accounts.models import Account

class PlantedTreeForm(forms.ModelForm):
    account = forms.ModelChoiceField(queryset=Account.objects.none())

    class Meta:
        model = PlantedTree
        fields = ['tree', 'age', 'latitude', 'longitude', 'account']
        widgets = {
            'tree': forms.Select(attrs={'class': 'form-control'}),
            'age': forms.NumberInput(attrs={'class': 'form-control'}),
            'latitude': forms.NumberInput(attrs={'class': 'form-control'}),
            'longitude': forms.NumberInput(attrs={'class': 'form-control'}),
            'account': forms.Select(attrs={'class': 'form-control'}),

        }
    
    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['tree'].queryset = Tree.objects.all()
        self.fields['account'].queryset = Account.objects.filter(users=user)