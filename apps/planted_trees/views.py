from django.views.generic.edit import CreateView
from django.urls import reverse_lazy
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from apps.planted_trees.forms import PlantedTreeForm
from apps.trees.models import Tree
from apps.planted_trees.models import PlantedTree
from apps.users.models import UserProfile

@login_required
def home_view(request):
    user_profile = UserProfile.objects.get(user=request.user)
    planted_trees = PlantedTree.objects.filter(user=user_profile)
    return render(request, 'planted_trees.html', {'planted_trees': planted_trees})


@login_required
def my_accounts(request):
    accounts = request.user.userprofile.accounts.all()
    planted_trees = PlantedTree.objects.filter(account__in=accounts)
    planted_trees = planted_trees.filter(user=request.user.userprofile)

    context = {'planted_trees': planted_trees}
    return render(request, 'my_accounts.html', context)


@login_required
def planted_tree_detail_view(request, id):
    user_profile = UserProfile.objects.get(user=request.user)
    tree = get_object_or_404(PlantedTree, id=id, user=user_profile)
    return render(request, 'planted_tree_detail.html', {'tree': tree})


class PlantedTreeCreateView(CreateView):
    model = PlantedTree
    form_class = PlantedTreeForm
    template_name = 'planted_tree_create.html'
    success_url = reverse_lazy('planted_trees:home')
    
    def form_valid(self, form):
        form.instance.user = self.request.user.userprofile
        return super().form_valid(form)
    
    def get_form(self, form_class=None):
        form = super().get_form(form_class=form_class)
        form.fields['tree'].queryset = Tree.objects.all()
        form.fields['account'].queryset = self.request.user.userprofile.accounts.all()
        return form
        
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user.userprofile
        return kwargs
