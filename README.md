# Trees Everywhere

Trees Everywhere é uma aplicação web, desenvolvida com Framework Django, que tem por objetivo criar um banco de dados de árvores plantadas por voluntários espalhados pelo mundo.

Entre as funcionalidades, pode se destacar:
- Cadastro de novos usuários e senha por um usuário administrador.
- Listagem e criação de contas por um usuário administrador.
- Cadastro e visualização de plantas por um usuário administrador.
- Efetuar login como usuário comum.
- Visualizar as árvores plantadas e suas informações como usuário comum.
- Adicionar uma árvore plantada como usuário comum.
- Exibir todas as árvores plantadas nas contas de que o usuário faz parte.
- Fornecer uma api REST para listar todas as árvores plantadas por um usuário logado.

## Como rodar o projeto?

* Clone esse repositório.
* Crie um virtualenv com Python 3.
* Ative o virtualenv.
* Instale as dependências.
* Rode as migrações.

```
git clone https://gitlab.com/lucasribeirorsousa/youshop-trees-everywhere-v1.git
cd youshop-trees-everywhere-v1
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
python contrib/env_gen.py
python manage.py migrate
python manage.py loaddata fixtures/*.json 
```

# Usuários para teste
- Admnistrador
```
    User = admin
    Password user = @adminAdmin123
```